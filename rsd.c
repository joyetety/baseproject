#include<stdio.h>
#include<stdarg.h>
#include<math.h>

double rsd(int num,...){
        va_list arg_ptr;
        va_start(arg_ptr,num);
        double sum = 0;
        int i;
        double array[num];
        for(i=0; i<num;i++){
                double tmp =  va_arg(arg_ptr,double);
                array[i] = tmp;
                sum += tmp;
        }
		va_end(arg_ptr);
        double avg = sum/num;
        double spow = 0;
        for(i=0; i<num;i++){
                spow += (array[i]-avg)*(array[i]-avg);
        }
        return (sqrt(spow/num))/avg;
}



int main(void){

        double sum = rsd(3,1.34f,1.39f,1.3f);
        printf("%e\n",sum);
        return 0;
}
